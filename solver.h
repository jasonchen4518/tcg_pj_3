#pragma once
#include <iostream>
#include <algorithm>
#include <cmath>
#include "board.h"
#include <numeric>
#include <vector>

double TT[531441][2][3][3][4];//TT[state][type][hint][min, avg, max][last_act]

class state_type {
public:
	enum type : char {
		before  = 'b',
		after   = 'a',
		illegal = 'i'
	};

public:
	state_type() : t(illegal) {}
	state_type(const state_type& st) = default;
	state_type(state_type::type code) : t(code) {}

	friend std::istream& operator >>(std::istream& in, state_type& type) {
		std::string s;
		if (in >> s) type.t = static_cast<state_type::type>((s + " ").front());
		return in;
	}

	friend std::ostream& operator <<(std::ostream& out, const state_type& type) {
		return out << char(type.t);
	}

	bool is_before()  const { return t == before; }
	bool is_after()   const { return t == after; }
	bool is_illegal() const { return t == illegal; }

private:
	type t;
};

class state_hint {
public:
	state_hint(const board& state) : state(const_cast<board&>(state)) {}

	char type() const { return state.info() ? state.info() + '0' : 'x'; }
	operator board::cell() const { return state.info(); }

public:
	friend std::istream& operator >>(std::istream& in, state_hint& hint) {
		while (in.peek() != '+' && in.good()) in.ignore(1);
		char v; in.ignore(1) >> v;
		hint.state.info(v != 'x' ? v - '0' : 0);
		return in;
	}
	friend std::ostream& operator <<(std::ostream& out, const state_hint& hint) {
		return out << "+" << hint.type();
	}

private:
	board& state;
};


class solver {
public:
	typedef float value_t;

public:
	class answer {
	public:
		answer(value_t min = 0.0/0.0, value_t avg = 0.0/0.0, value_t max = 0.0/0.0) : min(min), avg(avg), max(max) {}
	    friend std::ostream& operator <<(std::ostream& out, const answer& ans) {
	    	return !std::isnan(ans.avg) ? (out << ans.min << " " << ans.avg << " " << ans.max) : (out << "-1") /*<< std::endl*/;
		}
	public:
		const value_t min, avg, max;
	};

public:
	solver(const std::string& args) {
		// TODO: explore the tree and save the result
		int t;
		board tree;
		tree.bagcount = 1;	
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 1, 0);
		t = span(tree, state_type::after, 3, -1, 1, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 1, 2);
		t = span(tree, state_type::after, 3, -1, 1, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 1, 4);
		t = span(tree, state_type::after, 2, -1, 1, 5);
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 0, 0);
		t = span(tree, state_type::after, 3, -1, 0, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 0, 2);
		t = span(tree, state_type::after, 3, -1, 0, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 0, 4);
		t = span(tree, state_type::after, 2, -1, 0, 5);
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 2, 0);
		t = span(tree, state_type::after, 3, -1, 2, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 2, 2);
		t = span(tree, state_type::after, 3, -1, 2, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 2, 4);
		t = span(tree, state_type::after, 2, -1, 2, 5);
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 3, 0);
		t = span(tree, state_type::after, 3, -1, 3, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 3, 2);
		t = span(tree, state_type::after, 3, -1, 3, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 3, 4);
		t = span(tree, state_type::after, 2, -1, 3, 5);
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 4, 0);
		t = span(tree, state_type::after, 3, -1, 4, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 4, 2);
		t = span(tree, state_type::after, 3, -1, 4, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 4, 4);
		t = span(tree, state_type::after, 2, -1, 4, 5);
		
		tree.info(1);
		t = span(tree, state_type::after, 2, -1, 5, 0);
		t = span(tree, state_type::after, 3, -1, 5, 1);
		tree.info(2);
		t = span(tree, state_type::after, 1, -1, 5, 2);
		t = span(tree, state_type::after, 3, -1, 5, 3);
		tree.info(3);
		t = span(tree, state_type::after, 1, -1, 5, 4);
		t = span(tree, state_type::after, 2, -1, 5, 5);
		
		std::cout << "spanning over\n";
		
		
		
//		std::cout << "feel free to display some messages..." << std::endl;
	}

	answer solve(const board& state, state_type type = state_type::before) {
		// TODO: find the answer in the lookup table and return it
		//       do NOT recalculate the tree at here

		// to fetch the hint (if type == state_type::after, hint will be 0)
		int s = state(0) + state(1) * 9 + state(2) * 81 + state(3) * 729 + state(4) * 6561 + state(5) * 59049;
		int t;
		if(type.is_before()){
			t = 1;
		}
		else if(type.is_after()){
			t = 0;
		}

		if(t == 0){
			for(int i = 0; i < 4; i++){
				if(TT[s][t][state.info() - 1][0][i] > 0){
					answer a(TT[s][t][state.info() - 1][0][i], TT[s][t][state.info() - 1][1][i], TT[s][t][state.info() - 1][2][i]);
					return a;
				}
			}
			return {};
		}
		else{
			if(TT[s][t][state.info() - 1][0][0] > 0){
				answer a(TT[s][t][state.info() - 1][0][0], TT[s][t][state.info() - 1][1][0], TT[s][t][state.info() - 1][2][0]);
				return a;
			}
			else if(TT[s][t][state.info() - 1][0][0] == 0){
				return {};
			}
		}

		// for a legal state, return its three values.
//		return { min, avg, max };
		// for an illegal state, simply return {}
		return {};
	}
	
	int span(const board& b, state_type type, int hint, int act, int pos, int bt){
		board span_b = b;
		int child = -1, parent, tmp = -1, count = 0, tpa = -1;
		double avg = 0;
		if(type.is_before()){
			span_b.slide(act);

			parent = span_b(0) + span_b(1) * 9 + span_b(2) * 81 + span_b(3) * 729 + span_b(4) * 6561 + span_b(5) * 59049;
			if(TT[parent][0][hint - 1][0][act] != 0) return parent;
			switch(span_b.bagcount){
				case 0:
					for(int i = 0; i < 3; i++){
						switch(act){
							case 0:
								for(int j = 0; j < 3; j++){
									if(span_b(j + 3) == 0){
										child = span(span_b, state_type::after, i + 1, act, j + 3, i * 2);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0)
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][i][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][i][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][i][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
									}
								}
								break;
							case 1:
								for(int j = 0; j < 2; j++){
									if(span_b(j * 3) == 0){
										child = span(span_b, state_type::after, i + 1, act, j * 3, i * 2);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][i][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][i][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][i][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
									}
								}
								break;
							case 2:
								for(int j = 0; j < 3; j++){
									if(span_b(j) == 0){
										child = span(span_b, state_type::after, i + 1, act, j, i * 2);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][i][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][i][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][i][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
									}
								}
								break;
							case 3:
								for(int j = 0; j < 2; j++){
									if(span_b(j * 3 + 2) == 0){
										child = span(span_b, state_type::after, i + 1, act, j * 3 + 2, i * 2);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][i][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][i][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][i][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][i][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][i][2][0];
									}
								}
								break;
							default:
								return -1;
						}
					}
					break;
				case 1:
					for(int i = 0; i < 2; i++){
						switch(act){
							case 0:
								for(int j = 0; j < 3; j++){
									if(span_b(j + 3) == 0){
										child = span(span_b, state_type::after, span_b.bagarr[bt + i][span_b.bagcount], act, j + 3, bt + i);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
									}
								}
								break;
							case 1:
								for(int j = 0; j < 2; j++){
									if(span_b(j * 3) == 0){
										child = span(span_b, state_type::after, span_b.bagarr[bt + i][span_b.bagcount], act, j * 3, bt + i);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
									}
								}
								break;
							case 2:
								for(int j = 0; j < 3; j++){
									if(span_b(j) == 0){
										child = span(span_b, state_type::after, span_b.bagarr[bt + i][span_b.bagcount], act, j, bt + i);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
									}
								}
								break;
							case 3:
								for(int j = 0; j < 2; j++){
									if(span_b(j * 3 + 2) == 0){
										child = span(span_b, state_type::after, span_b.bagarr[bt + i][span_b.bagcount], act, j * 3 + 2, bt + i);
										count++;
										if(TT[parent][0][hint - 1][0][act] == 0) 
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0])
											TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][0][0];
										TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][1][0];
										if(TT[parent][0][hint - 1][2][act] == 0) 
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
										else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0])
											TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt + i][span_b.bagcount] - 1][2][0];
									}
								}
								break;
							default:
								return -1;
						}
					}
					break;
				case 2:
					switch(act){
						case 0:
							for(int j = 0; j < 3; j++){
								if(span_b(j + 3) == 0){
									child = span(span_b, state_type::after, span_b.bagarr[bt][span_b.bagcount], act, j + 3, bt);
									count++;
									if(TT[parent][0][hint - 1][0][act] == 0) 
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0])
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][1][0];
									if(TT[parent][0][hint - 1][2][act] == 0) 
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
									else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0])
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
								}
							}
							break;
						case 1:
							for(int j = 0; j < 2; j++){
								if(span_b(j * 3) == 0){
									child = span(span_b, state_type::after, span_b.bagarr[bt][span_b.bagcount], act, j * 3, bt);
									count++;
									if(TT[parent][0][hint - 1][0][act] == 0) 
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0])
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][1][0];
									if(TT[parent][0][hint - 1][2][act] == 0) 
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
									else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0])
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
								}
							}
							break;
						case 2:
							for(int j = 0; j < 3; j++){
								if(span_b(j) == 0){
									child = span(span_b, state_type::after, span_b.bagarr[bt][span_b.bagcount], act, j, bt);
									count++;
									if(TT[parent][0][hint - 1][0][act] == 0) 
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0])
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][1][0];
									if(TT[parent][0][hint - 1][2][act] == 0) 
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
									else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0])
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
								}
							}
							break;
						case 3:
							for(int j = 0; j < 2; j++){
								if(span_b(j * 3 + 2) == 0){
									child = span(span_b, state_type::after, span_b.bagarr[bt][span_b.bagcount], act, j * 3 + 2, bt);
									count++;
									if(TT[parent][0][hint - 1][0][act] == 0) 
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									else if(TT[parent][0][hint - 1][0][act] > TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0])
										TT[parent][0][hint - 1][0][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][0][0];
									TT[parent][0][hint - 1][1][act] += TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][1][0];
									if(TT[parent][0][hint - 1][2][act] == 0) 
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];
									else if(TT[parent][0][hint - 1][2][act] < TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0])
										TT[parent][0][hint - 1][2][act] = TT[child][1][span_b.bagarr[bt][span_b.bagcount] - 1][2][0];						}
							}
							break;
						default:
							return -1;
					}
					break;
				default:
					return -1;
			}
			TT[parent][0][hint - 1][1][act] = TT[parent][0][hint - 1][1][act] / count;
		}
		else if(type.is_after()){
			span_b(pos) = span_b.info(hint);
			parent = span_b(0) + span_b(1) * 9 + span_b(2) * 81 + span_b(3) * 729 + span_b(4) * 6561 + span_b(5) * 59049;
			if(TT[parent][1][hint - 1][0][0] != 0) return parent;
			if(span_b.bagcount == 2){
				span_b.bagcount = 0;
			}
			else{
				span_b.bagcount++;
			}
			for(int i = 0; i < 4; i++){
				board::reward reward = board(span_b).slide(i);
				if(reward != -1){
					child = span(span_b, state_type::before, hint, i, -1, bt);
					
					if(TT[child][0][hint - 1][1][i] > avg){
						avg = TT[child][0][hint - 1][1][i];
						tmp = child;
						tpa = i;
					}
				}
			}
			child = tmp;
			if(child == -1){
				int value = 0;
				for(int i = 0; i < 6; i++){
					if(span_b(i) >= 3)
					value += std::pow(3, span_b(i) - 2);
				}
				TT[parent][1][hint - 1][0][0] = value;
				TT[parent][1][hint - 1][1][0] = value;
				TT[parent][1][hint - 1][2][0] = value;
			}
			else if(child > 0){
				TT[parent][1][hint - 1][0][0] = TT[child][0][hint - 1][0][tpa];
				TT[parent][1][hint - 1][1][0] = TT[child][0][hint - 1][1][tpa];
				TT[parent][1][hint - 1][2][0] = TT[child][0][hint - 1][2][tpa];
			}
		}
		return parent;
	}

private:
	// TODO: place your transposition table here
	//type 1:before 0:after
};
